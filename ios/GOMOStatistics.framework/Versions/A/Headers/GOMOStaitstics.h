//
//  GOMOStaitstics.h
//  Pods
//
//  Created by Zhou Derain on 2018/5/31.
//

#import <Foundation/Foundation.h>

@interface GOMOStaitstics : NSObject

/*********************************统计SDK配置*****************************************/

/**
 配置SDK功能点 (统一配置)
 
 @param paymentFuncId               支付统计协议功能点
 @param disseminationFuncId         推广统计协议功能点
 @param userRealTimeBehaviorFuncId  用户实时行为统计协议功能点   (统计数据实时上传)
 @param userNormalBehaviorFuncId    用户行为统计协议功能点      (统计数据8小时上传一次)
 @param adFuncId                    广告接入统计协议功能点
 */
+ (void)configurePaymentFuncId:(NSString *)paymentFuncId
           disseminationFuncId:(NSString *)disseminationFuncId
             userRealTimeBehaviorFuncId:(NSString *)userRealTimeBehaviorFuncId
             userNormalBehaviorFuncId:(NSString *)userNormalBehaviorFuncId
             adFuncId:(NSString *)adFuncId;


/**
 配置SDK功能点 (可单独配置)

 @param paymentFuncId 支付统计协议功能点
 */
+ (void)configurePaymentFuncId:(NSString *)paymentFuncId;


/**
 配置SDK功能点 (可单独配置)

 @param disseminationFuncId 推广统计协议功能点
 */
+ (void)configureDisseminationFuncId:(NSString *)disseminationFuncId;


/**
 配置SDK功能点 (可单独配置)

 @param userRealTimeBehaviorFuncId 用户实时行为统计协议功能点 (统计数据实时上传)
 */
+ (void)configureUserRealTimeBehaviorFuncId:(NSString *)userRealTimeBehaviorFuncId;


/**
 配置SDK功能点 (可单独配置)

 @param userNormalBehaviorFuncId 用户行为统计协议功能点      (统计数据8小时上传一次)
 */
+ (void)configureUserNormalBehaviorFuncId:(NSString *)userNormalBehaviorFuncId;


/**
 配置SDK功能点 (可单独配置)

 @param adFuncId 广告接入统计协议功能点
 */
+ (void)configureAdFuncId:(NSString *)adFuncId;



/**
 初始化, 便于初次上传用户信息与日志;
 */
+ (void)setup;


/*********************************埋点统计方法******************************************/

/**
 上传用于信息 (19协议)
 */
+ (void)uploadUserInfoStatisticsIfNeeded;


/**
 上传合作方推广统计 (45协议)
 
 @param utmSource 统计对象
 @param success 操作结果
 @param afDetail AF明细
 @param reffer Referrer
 @param afAgency AF Agency
 @param userSource 用户类型标识
 @param appsflyVersion 推广SDK版本
 */
+ (void)postUserDissemniationStatisticWithUtmSource:(NSString *)utmSource
                                            success:(NSString *)success
                                           afDetail:(NSString *)afDetail
                                             reffer:(NSString *)reffer
                                           afAgency:(NSString *)afAgency
                                         userSource:(NSString *)userSource
                                     appsflyVersion:(NSString *)appsflyVersion;


/**
 插入一条行为统计记录(缓存到本地数据库)(101协议)
 
 @param operationCode 操作码
 @param statisticsObject 统计对象
 @param associationObject 关联对象
 @param tab Tab分类
 @param entrance 入口标注
 @param remark 备注
 @param position 位置
 */
+ (void)insertUserStatisticOperationCode:(NSString *)operationCode
                        statisticsObject:(NSString *)statisticsObject
                       associationObject:(NSString *)associationObject
                                     tab:(NSString *)tab
                                entrance:(NSString *)entrance
                                  remark:(NSString *)remark
                                position:(NSString *)position;

/**
 上传一条实时统计 (即时上传服务器)(104协议)
 
 @param operationCode 操作码
 @param statisticsObject 统计对象
 @param associationObject 关联对象
 @param tab Tab分类
 @param remark 备注
 @param position 位置
 */
+ (void)postUserRealtimeStatisticOperationCode:(NSString *)operationCode
                              statisticsObject:(NSString *)statisticsObject
                             associationObject:(NSString *)associationObject
                                           tab:(NSString *)tab
                                        remark:(NSString *)remark
                                      position:(NSString *)position;

/**
 上传一条广告实时统计 (即时上传服务器)(105协议)
 
 @param operationCode 操作码
 @param statisticsObject 统计对象
 @param associationObject 关联对象
 @param tab Tab分类
 @param remark 备注
 @param position 位置
 @param adId 广告Id
 */
+ (void)postAdRealtimeStatisticOperationCode:(NSString *)operationCode
                            statisticsObject:(NSString *)statisticsObject
                           associationObject:(NSString *)associationObject
                                         tab:(NSString *)tab
                                      remark:(NSString *)remark
                                    position:(NSString *)position adId:(NSString *)adId;


/**
 支付记录实时上传 (即时上传服务器) (59协议)功能点
 
 @param operationCode 操作码
 @param statisticsObject 统计对象
 @param associationObject 关联对象
 @param tab Tab分类
 @param orderID 第三方订单ID
 @param entrance 入口
 @param remark GLive 相关业务，上传内部订单号，其他产品忽略
 */
+ (void)postUserRealtimePayMentStatisticOperationCode:(NSString *)operationCode
                                     statisticsObject:(NSString *)statisticsObject
                                    associationObject:(NSString *)associationObject
                                                  tab:(NSString *)tab
                                              orderID:(NSString *)orderID
                                             entrance:(NSString *)entrance
                                               remark:(NSString *)remark;

@end
