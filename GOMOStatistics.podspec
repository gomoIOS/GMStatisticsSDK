Pod::Spec.new do |s|
  s.name = "GOMOStatistics"
  s.version = "0.2.3"
  s.summary = "A lib to statistics user behavior."
  s.license = {"type"=>"MIT", "file"=>"LICENSE"}
  s.authors = {"ZDerain"=>"derain-zhou@foxmail.com"}
  s.homepage = "https://gitlab.com/gomo_sdk/GOMOStaitstics"
  s.description = "GOMO\u7EDF\u8BA1SDK; \u652F\u6301 \u652F\u4ED8\u7EDF\u8BA1, \u63A8\u5E7F\u7EDF\u8BA1, \u7528\u6237\u884C\u4E3A\u7EDF\u8BA1, \u7528\u6237\u5B9E\u65F6\u884C\u4E3A\u7EDF\u8BA1, \u7528\u6237\u4FE1\u606F\u7EDF\u8BA1 , \u5E7F\u544A\u7EDF\u8BA1."
  s.frameworks = ["UIKit", "Foundation", "SystemConfiguration"]
  s.libraries = ["resolv", "sqlite3"]
  s.source = { :path => '.' }  

  s.ios.deployment_target    = '8.0'
  s.ios.vendored_framework   = 'ios/GOMOStatistics.framework'
end
